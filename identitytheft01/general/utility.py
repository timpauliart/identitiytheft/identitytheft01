import itertools as it


def walk_deterministic_until_cond(chain, start, single_cond, whole_cond):
    generator = chain.walk_deterministic(start)
    result = []
    while not whole_cond(result):
        element = next(generator)
        if single_cond(element):
            result.append(element)
    return result


def scale(value, mn, mx, result_min, result_max):
    old_range = mx - mn
    if old_range == 0:
        old_range = 0.5
    new_range = result_max - result_min
    return (((value - mn) * new_range) / old_range) + result_min


def scale_ls(values, result_min, result_max):
    mx = max(values)
    mn = min(values)
    return [scale(value, mn, mx, result_min, result_max) for value in values]


def stretch(values, aim):
    thesum = sum(values)
    factor = aim / thesum
    return [factor * v for v in values]


def delays_to_times(delays, starttime=0):
    times = tuple(it.accumulate(delays))
    times = [starttime] + [time + starttime for time in times[:-1]]
    return times


def quantize(value, quant):
    result = quant * round(value / quant)
    return result


def conditional_split(iterable, cond=lambda ls, i: len(ls) > 0):
    result = []
    group = []
    for i in iterable:
        if cond(group, len(result)):
            result.append(group)
            group = []
        group.append(i)
    if len(group) < 0:
        result.append(group)
    return result


def make_minsum_condition(minsums, key=lambda x: x):
    def cond(ls, i):
        minsum = minsums[i]
        key_ls = [key(l) for l in ls]
        return sum(key_ls) >= minsum

    return cond


def transform_according(
    ls,
    key=lambda ls: sum(ls),
    minmax=lambda ls: min(ls),
    transform=lambda ls, minmax: ls,
):
    key_ls = [key(l) for l in ls]
    minmax = minmax(key_ls)
    result = [transform(ls, minmax) for l in ls]
    return result
