import csv
import pathlib as pl
import yamm
import functools as ft


def read_event_csv(csvname):
    with open(csvname, "r") as f:
        reader = csv.reader(f)
        result = tuple((x[0], float(x[1]), float(x[2])) for x in reader)
        return result


def read_tempo_csv(csvname):
    with open(csvname, "r") as f:
        reader = csv.reader(f)
        result = float(tuple(reader)[0][0])
        return result


def create_chain(folder, exp, order=1):
    sequences = get_events(folder, exp)
    chains = []
    for s in sequences:
        values = tuple(x[:2] for x in s)
        chain = yamm.Chain.from_data(values, order)
        chains.append(chain)
    result = ft.reduce(lambda x, y: x.merge(y), chains)
    result.make_deterministic_map()
    return result


def get_events(folder, exp):
    csvnames = sorted(pl.Path(folder).rglob(exp))
    result = []
    for name in csvnames:
        values = read_event_csv(name)
        result.append(values)
    return result


def get_tempi(folder, exp):
    csvnames = sorted(pl.Path(folder).rglob(exp))
    result = []
    for name in csvnames:
        print(str(name))
        value = read_tempo_csv(name)
        result.append(value)
    return result
