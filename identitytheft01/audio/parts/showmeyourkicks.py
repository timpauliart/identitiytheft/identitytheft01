import os
import pathlib as pl
import itertools as it

import identitytheft01.audio as aud
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl


def create_showmeyourkicks(server, rhy_chain, tempynamic):
    """lots of kicks and 808s"""

    # get the soundfiles
    kickfolder = os.getcwd() + "/samples/kick"
    bassfolder = os.getcwd() + "/samples/808"
    kickfiles = sorted([str(f) for f in pl.Path(kickfolder).glob("*")])
    bassfiles = sorted([str(f) for f in pl.Path(bassfolder).glob("*")])
    # soundfiles = sorted([str(f) for f in kickfiles + bassfiles])

    start = (("kick", 1 / 32.0),)
    whole_check = sutl.get_whole_len_checker(len(kickfiles) * 2)
    single_check = sutl.get_single_dur_checker(0, 1 / 12.0, None, False)
    rhy = gutl.walk_deterministic_until_cond(
        rhy_chain, start, single_check, whole_check
    )
    rhy = [r[1] for r in rhy]

    kick_rhy1 = tuple(it.islice(rhy, 0, None, 2))
    bass_rhy1 = tuple(it.islice(rhy, 1, None, 2))
    kick_rhy2 = [k + b for k, b in zip(kick_rhy1, bass_rhy1)] + [32]
    bass_rhy2 = [k + b for k, b in zip(kick_rhy1[1:], bass_rhy1)] + [32]

    kickfiles.append(bassfiles[-1])

    duration = 30
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 0.25
    durfactor = 0.5

    aud.render_samples(
        server=server,
        outfilename="1showmeyourkicks",
        outfiledur=duration,
        bpm=bpm,
        delays=kick_rhy2,
        delaysfactor=speedfactor,
        soundfile=kickfiles,
        durfactor=durfactor,
    )

    aud.render_samples(
        server=server,
        outfilename="1showmeyour808s",
        outfiledur=duration,
        bpm=bpm,
        delays=bass_rhy2,
        delaysfactor=speedfactor,
        soundfile=bassfiles,
        durfactor=durfactor,
    )
