import os
import pathlib as pl

import identitytheft01.audio as aud
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl


def create_showmeyoursnares(server, rhy_chain, tempynamic):
    """lots of snares"""

    # get the soundfiles
    snarefolder = os.getcwd() + "/samples/snare"
    snarefiles = sorted([str(f) for f in pl.Path(snarefolder).glob("*")])

    start = (("snare", 1 / 32.0),)
    whole_check = sutl.get_whole_len_checker(len(snarefiles))
    single_check = sutl.get_single_dur_checker(0, 1 / 3.0, None, False)
    rhy = gutl.walk_deterministic_until_cond(
        rhy_chain, start, single_check, whole_check
    )
    rhy = [r[1] for r in rhy] + [32]

    gun = os.getcwd() + "/samples/fx/realworld/Gun_Cockback.wav"
    snarefiles.append(gun)

    duration = 30
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 0.125
    durfactor = 0.9

    aud.render_samples(
        server=server,
        outfilename="5showmeyoursnares",
        outfiledur=duration,
        bpm=bpm,
        delays=rhy,
        delaysfactor=speedfactor,
        soundfile=snarefiles,
        durfactor=durfactor,
    )
