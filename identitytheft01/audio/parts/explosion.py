import os
import pathlib as pl
import itertools as it

import identitytheft01.audio as aud

import identitytheft01.score.utility as sutl
import identitytheft01.general.utility as gutl


def create_explosion(
    server, drum_chain, bass_chain, snare_chain, hihat_chain, bar_chain, tempynamic
):
    # files
    adlib_folder = os.getcwd() + "/samples/adlib"
    adlib_files = it.cycle(sorted([str(f) for f in pl.Path(adlib_folder).glob("*")]))
    perc_folder = os.getcwd() + "/samples/perc"
    perc_files = it.cycle(sorted([str(f) for f in pl.Path(perc_folder).glob("*")]))
    bass_folder = os.getcwd() + "/samples/808"
    bass_files = it.cycle(sorted([str(f) for f in pl.Path(bass_folder).glob("*")]))
    kick_folder = os.getcwd() + "/samples/kick"
    kick_files = it.cycle(sorted([str(f) for f in pl.Path(kick_folder).glob("*")]))
    snare_folder = os.getcwd() + "/samples/snare"
    snare_files = it.cycle(sorted([str(f) for f in pl.Path(snare_folder).glob("*")]))
    hihat_folder = os.getcwd() + "/samples/hihat"
    hihat_files = it.cycle(sorted([str(f) for f in pl.Path(hihat_folder).glob("*")]))
    drumfile_dic = {"kick": kick_files, "snare": snare_files, "hihat": hihat_files}

    # general stuff

    whole_check = sutl.get_whole_dur_checker(32)
    single_check = sutl.get_single_dur_checker(0, 32, None, False)

    # bars

    bar_start = sorted(bar_chain.keys(), key=lambda key: sum(k[1] for k in key))[-1]
    bar_durations = gutl.walk_deterministic_until_cond(
        bar_chain, bar_start, single_check, whole_check
    )

    bar_cond = gutl.make_minsum_condition(
        [x[1] for x in bar_durations], key=lambda x: x[1]
    )

    duration = 90
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 0.25

    # drums
    drum_start = list(
        filter(
            lambda x: x[0][0] == "kick",
            sorted(drum_chain.keys(), key=lambda key: sum(k[1] for k in key)),
        )
    )[0]

    drum_events = gutl.walk_deterministic_until_cond(
        drum_chain, drum_start, single_check, whole_check
    )
    splitted_drum_events = gutl.conditional_split(drum_events, bar_cond)
    delays = []
    files = []
    loops = []
    for drum_bar, samples in zip(splitted_drum_events, it.cycle(["drum", "perc"])):
        tmp_delays = [e[1] for e in drum_bar]
        if samples == "drum":
            tmp_files = [next(drumfile_dic[e[0]]) for e in drum_bar]
            tmp_loops = [0 for e in drum_bar]
        else:
            sample = next(perc_files)
            tmp_files = [sample for e in drum_bar]
            tmp_loops = [1 for e in drum_bar]
        delays.extend(tmp_delays)
        files.extend(tmp_files)
        loops.extend(tmp_loops)

    delays = delays + [32]
    files = (
        # [os.getcwd() + "/samples/fx/siren/Siren.wav"] # TODO dont fortget for now manual daw
        files
        + [os.getcwd() + "/samples/fx/realworld/araabMUZIK_Scream.wav"]
    )
    loops = loops + [0]

    aud.render_samples(
        server=server,
        outfilename="35explosion_drums",
        outfiledur=duration,
        bpm=bpm,
        delays=delays,
        delaysfactor=speedfactor,
        soundfile=files,
        loopsample=loops,
    )

    # adlibs
    ad_start = list(
        filter(
            lambda x: x[0][0] == "hihat",
            sorted(drum_chain.keys(), key=lambda key: sum(k[1] for k in key)),
        )
    )[0]

    ad_events = gutl.walk_deterministic_until_cond(
        drum_chain, ad_start, single_check, whole_check
    )
    splitted_ad_events = gutl.conditional_split(ad_events, bar_cond)

    delays = []
    files = []
    loops = []
    for bar, samples in zip(splitted_ad_events, it.cycle(["drum", "perc"])):
        tmp_delays = [e[1] for e in bar]
        if samples == "drum":
            tmp_files = [next(adlib_files) for e in bar]
            tmp_loops = [0 for e in bar]
            delays.extend(tmp_delays)
            files.extend(tmp_files)
            loops.extend(tmp_loops)
        else:
            delays[-1] += sum(tmp_delays)

    aud.render_samples(
        server=server,
        outfilename="35explosion_adlibs",
        outfiledur=duration,
        bpm=bpm,
        delays=delays,
        delaysfactor=speedfactor,
        soundfile=files,
        loopsample=loops,
    )

    render_drum_stem(
        bass_chain,
        bass_files,
        "35explosion_808",
        bar_cond,
        whole_check,
        server,
        duration,
        bpm,
        speedfactor,
    )
    render_drum_stem(
        snare_chain,
        snare_files,
        "35explosion_snare",
        bar_cond,
        whole_check,
        server,
        duration,
        bpm,
        speedfactor,
    )
    render_drum_stem(
        hihat_chain,
        hihat_files,
        "35explosion_hihat",
        bar_cond,
        whole_check,
        server,
        duration,
        bpm,
        speedfactor,
    )


def render_drum_stem(
    chain,
    in_files,
    filename,
    bar_cond,
    whole_check,
    server,
    fileduration,
    bpm,
    speedfactor,
):
    start = list(
        filter(
            lambda x: x[0][1] <= 4,
            sorted(chain.keys(), key=lambda key: sum(k[1] for k in key)),
        )
    )[-1]

    single_check = sutl.get_single_dur_checker(0, 4, None, False)

    events = gutl.walk_deterministic_until_cond(chain, start, single_check, whole_check)
    splitted_events = gutl.conditional_split(events, bar_cond)

    delays = []
    files = []
    loops = []
    durfactors = []
    for bar, samples in zip(splitted_events, it.cycle(["drum", "perc"])):
        tmp_delays = [e[1] for e in bar]
        if samples == "drum":
            tmp_files = [next(in_files) for e in bar]
            tmp_loops = [0 for e in bar]
            tmp_durfactors = [1 for e in bar]
            delays.extend(tmp_delays)
            files.extend(tmp_files)
            loops.extend(tmp_loops)
            durfactors.extend(tmp_durfactors)
        else:
            delays[-1] += sum(tmp_delays)
            durfactors[-1] = 0.1

    aud.render_samples(
        server=server,
        outfilename=filename,
        outfiledur=fileduration,
        bpm=bpm,
        delays=delays,
        delaysfactor=speedfactor,
        soundfile=files,
        loopsample=loops,
        durfactor=durfactors,
    )
