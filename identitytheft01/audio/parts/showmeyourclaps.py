import os
import pathlib as pl

import identitytheft01.audio as aud


def create_showmeyourclaps(server, tempynamic):
    """lots of claps"""

    # get the soundfiles
    clapfolder = os.getcwd() + "/samples/clap"
    clapfiles = sorted([str(f) for f in pl.Path(clapfolder).glob("*")])

    rhy = [0.0625 for i in range(len(clapfiles))] + [32]

    riser = os.getcwd() + "/samples/fx/riser/FIRST_EVER_BEAT_SHIT_3.wav"
    clapfiles.append(riser)

    duration = 60
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 1
    durfactor = 1

    aud.render_samples(
        server=server,
        outfilename="6showmeyourclaps",
        outfiledur=duration,
        bpm=bpm,
        delays=rhy,
        delaysfactor=speedfactor,
        soundfile=clapfiles,
        durfactor=durfactor,
    )
