import os
import pathlib as pl

import identitytheft01.audio as aud
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl


def create_showmeyourhihats(server, rhy_chain, tempynamic):
    """lots of hihats"""

    # get the soundfiles
    hihatfolder = os.getcwd() + "/samples/hihat"
    hihatfiles = sorted([str(f) for f in pl.Path(hihatfolder).glob("*")])

    start = (("hihat", 1 / 32.0),)
    whole_check = sutl.get_whole_len_checker(len(hihatfiles))
    single_check = sutl.get_single_dur_checker(0, 1 / 6.0, None, False)
    rhy = gutl.walk_deterministic_until_cond(
        rhy_chain, start, single_check, whole_check
    )
    rhy = [r[1] for r in rhy] + [32]

    hi = os.getcwd() + "/samples/fx/hi/NOTHING_NEW_HERE_1.wav"
    hihatfiles.append(hi)

    duration = 30
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 0.125
    durfactor = 0.8

    aud.render_samples(
        server=server,
        outfilename="3showmeyourhihats",
        outfiledur=duration,
        bpm=bpm,
        delays=rhy,
        delaysfactor=speedfactor,
        soundfile=hihatfiles,
        durfactor=durfactor,
    )
