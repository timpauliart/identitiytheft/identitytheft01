class Event:
    def __init__(
        self,
        instr: str,
        delay: float,
        dur: float = None,
        dyn: float = None,
        arco: bool = False,
    ) -> None:
        self.instr = instr
        # hack
        self.__duration = delay
        #
        self.delay = delay
        self.duration = dur
        self.dynamic = dyn
        self.arco = arco

    @property
    def instr(self) -> str:
        return self.__instr

    @instr.setter
    def instr(self, instr: str):
        self.__instr = instr

    @property
    def delay(self) -> float:
        return self.__delay

    @delay.setter
    def delay(self, delay: float):
        assert delay >= 0
        assert self.__duration is None or delay >= self.duration
        self.__delay = delay

    @property
    def duration(self) -> float:
        if self.__duration is None:
            return self.delay
        else:
            return self.__duration

    @duration.setter
    def duration(self, duration: float):
        assert duration is None or duration >= 0
        assert duration is None or duration <= self.delay
        self.__duration = duration

    @property
    def dynamic(self) -> float:
        return self.__dynamic

    @dynamic.setter
    def dynamic(self, dyn: float):
        assert dyn is None or dyn in [0, 1, 2, 3, 4, 5, 6, 7]
        self.__dynamic = dyn

    @property
    def arco(self) -> bool:
        return self.__arco

    @arco.setter
    def arco(self, arco: bool):
        self.__arco = arco

    def __repr__(self) -> str:
        return (
            "("
            + str(self.instr)
            + " "
            + str(self.delay)
            + " "
            + str(self.duration)
            + " "
            + str(self.dynamic)
            + " "
            + str(self.arco)
            + ")"
        )
