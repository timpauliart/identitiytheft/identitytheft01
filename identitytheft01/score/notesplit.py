import abjad
import bisect
import functools
import itertools
import operator

from .crosstrainer import Stack

import fractions


# by big master levin zimmermann


def seperate_by_grid(
    start: fractions.Fraction, stop: fractions.Fraction, grid: tuple,
) -> tuple:
    absolute_grid = (0,) + tuple(itertools.accumulate(grid))
    grid_start = bisect.bisect_right(absolute_grid, start) - 1
    grid_stop = bisect.bisect_right(absolute_grid, stop)
    passed_groups = tuple(range(grid_start, grid_stop, 1))
    if len(passed_groups) == 1:
        return (stop - start,)

    else:
        delays = []
        is_connectable_per_delay = []
        for i, group in enumerate(passed_groups):
            if i == 0:
                diff = start - absolute_grid[group]
                new_delay = grid[group] - diff
                is_connectable = any(
                    (start in absolute_grid, new_delay.denominator <= 1)
                )
            elif i == len(passed_groups) - 1:
                new_delay = stop - absolute_grid[group]
                is_connectable = any(
                    (stop in absolute_grid, new_delay.denominator <= 1)
                )
            else:
                new_delay = grid[group]
                is_connectable = True

            if new_delay > 0:
                delays.append(new_delay)
                is_connectable_per_delay.append(is_connectable)

        ldelays = len(delays)
        if ldelays == 1:
            return tuple(delays)

        connectable_range = [int(not is_connectable_per_delay[0]), ldelays]
        if not is_connectable_per_delay[-1]:
            connectable_range[-1] -= 1

        solutions = [((n,), m) for n, m in enumerate(delays)]
        for item0 in range(*connectable_range):
            for item1 in range(item0 + 1, connectable_range[-1] + 1):
                connected = sum(delays[item0:item1])
                if abjad.Duration(connected).is_assignable:
                    sol = (tuple(range(item0, item1)), connected)
                    if sol not in solutions:
                        solutions.append(sol)

        possibilites = Stack(fitness="min")
        amount_connectable_items = len(delays)
        has_found = False
        lsolrange = tuple(range(len(solutions)))
        for combsize in range(1, amount_connectable_items + 1):
            for comb in itertools.combinations(lsolrange, combsize):
                pos = tuple(solutions[idx] for idx in comb)
                items = functools.reduce(operator.add, tuple(p[0] for p in pos))
                litems = len(items)
                is_unique = litems == len(set(items))
                if is_unique and litems == amount_connectable_items:
                    sorted_pos = tuple(
                        item[1] for item in sorted(pos, key=lambda x: x[0][0])
                    )
                    fitness = len(sorted_pos)
                    possibilites.append(sorted_pos, fitness)
                    has_found = True
            if has_found:
                break
        result = possibilites.best[0]
        return tuple(result)


def seperate_by_assignability(duration, max_duration) -> tuple:
    def find_sum_in_numbers(numbers, solution) -> tuple:
        result = []
        # from smallest biggest to smallest
        nums = reversed(sorted(numbers))
        current_num = next(nums)
        while sum(result) != solution:
            if sum(result) + current_num <= solution:
                result.append(current_num)
            else:
                current_num = next(nums)
        return result

    # easy claim for standard note duration
    if abjad.Duration(duration).is_assignable and duration <= max_duration:
        return (abjad.Duration(duration),)

    # top and bottom
    numerator = duration.numerator
    denominator = duration.denominator

    # we only need note durations > 1 / denominator
    possible_durations = [
        fractions.Fraction(i, denominator)
        for i in range(1, numerator + 1)
        # only standard note durations
        if abjad.Duration(i, denominator).is_assignable
        and (i / denominator) <= max_duration
    ]

    # find the right combination
    solution = find_sum_in_numbers(possible_durations, duration)
    return solution
