import abjad as aj
from .beautify import beautify


def create_score(notes):
    staff = aj.Staff(notes)
    # aj.attach(aj.LilyPondLiteral(r"\new Drumstaff", "before"), staff)
    # score
    # instruments
    order = [
        "clave",
        "cymbal",
        "cowbell",
        "conga",
        "snare drum",
        "floor tom",
        "bass drum",
    ]
    order = [aj.Markup(x) for x in order]
    # aj.setting(staff).instrument_name = aj.Markup.column(order)
    # lines
    aj.override(staff).staff_symbol.line_count = 7
    aj.override(staff).staff_symbol.line_positions = "#'(-6 -4 -2 0 2 4 6)"
    # aj.override(staff).TextScript.staff_padding = 2

    first_leaf = aj.inspect(staff).leaf(0)
    # clef
    clef = aj.Clef("percussion")
    aj.attach(clef, first_leaf)
    # numeric
    numeric = aj.LilyPondLiteral(r"\numericTimeSignature", "before")
    aj.attach(numeric, first_leaf)
    # rehearsal marks
    box = r"\set Score.markFormatter = #format-mark-box-alphabet"
    mark_style = aj.LilyPondLiteral(box, "before")
    aj.attach(mark_style, first_leaf)
    # beautify
    beautify(staff)

    # score
    score = aj.Score([staff])
    score.add_final_bar_line()

    # header
    lilypond_file = aj.LilyPondFile.new(score)
    title = "identity theft 01"
    lilypond_file.header_block.title = aj.Markup(title)
    subtitle = "drums, computer, speakers, video"
    lilypond_file.header_block.subtitle = aj.Markup(subtitle)
    lilypond_file.header_block.tagline = aj.Markup(
        "gitlab.com/timpauliart/identitytheft/identitytheft01"
    )
    lilypond_file.header_block.composer = aj.Markup("Tim Pauli")
    lilypond_file.header_block.copyright = aj.Markup("2020")

    # proportional notation
    prop = r"\context {\Score proportionalNotationDuration = #(ly:make-moment 1/8)}"
    lilypond_file.layout_block.items.append(prop)
    # minimum = "system-system-spacing #'minimum-distance = #16"
    # lilypond_file.paper_block.items.append(minimum)

    # render
    folder = "build/"
    clean_title = title.replace(" ", "")
    pdf_suffix = "_score.pdf"
    aj.persist(lilypond_file).as_pdf(folder + clean_title + pdf_suffix)
    midi_suffix = ".midi"
    aj.persist(lilypond_file).as_midi(folder + clean_title + midi_suffix)
