import abjad as aj

import identitytheft01.score.event as e
import identitytheft01.score.utility as sutl
from identitytheft01.score.notemaker import NoteMaker as nm


def create_showmeyourrolls(tempynamic1, tempynamic2, tape_counter):
    """roll"""
    # open the snare fbi
    open_snare = (e.Event(None, 1),)
    open_snare = nm.make_notes(open_snare)
    openit = "snare on"
    cmdopen = aj.Markup(openit, direction=aj.Up,)
    aj.attach(cmdopen, open_snare[0])

    # roll by hand
    drum_nhits = 228  # TODO make automatic number of hits
    events1 = [e.Event("sd", 1 / 16.0) for i in range(drum_nhits)]
    events1[0].dynamic = 6
    sutl.append_rest_eob(events1)
    notes1 = nm.make_notes(events1)
    # attach by hand
    tempo_mark1 = aj.MetronomeMark((1, 4), tempynamic1[0])
    aj.attach(tempo_mark1, notes1[0])
    tape1 = next(tape_counter)
    aj.attach(tape1, notes1[0])
    tape2 = next(tape_counter)
    aj.attach(tape2, notes1[-1])
    aj.attach(aj.Fermata(command="longfermata"), notes1[-1])

    # roll by milk
    events2 = [e.Event("sd", (drum_nhits / 2.0) * 1 / 16.0, arco=True)]
    events2[0].dynamic = 3
    sutl.append_rest_eob(events2)
    notes2 = nm.make_notes(events2)
    # attach by milk
    tempo_mark2 = aj.MetronomeMark((1, 4), tempynamic2[0])
    aj.attach(tempo_mark2, notes2[0])
    milk_on_str = "milk frother on"
    milk_on = aj.Markup(milk_on_str, direction=aj.Up,)
    aj.attach(milk_on, notes2[0])
    milk_off_str = "milk frother off"
    milk_off = aj.Markup(milk_off_str, direction=aj.Up,)
    aj.attach(milk_off, notes2[-2])

    voice = aj.Voice(open_snare + notes1 + notes2)
    result = aj.Container([voice])
    return result
