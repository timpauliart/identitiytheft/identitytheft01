import itertools as it

import abjad as aj

import identitytheft01.score.event as e
import identitytheft01.score.utility as sutl
import identitytheft01.general.utility as gutl
from identitytheft01.score.notemaker import NoteMaker as nm


def create_showmeyoursounds(
    rhy_chain, bar_chain, tempynamic1, tempynamic2, tape_counter
):
    """ladder"""

    # get pseudo bars
    bar_start = (("kick", 1),)
    bar_check = sutl.get_whole_dur_checker(16)
    bar_filter = sutl.get_single_dur_checker(0.5, 32, 0.125, False)
    bar_durations1 = gutl.walk_deterministic_until_cond(
        bar_chain, bar_start, bar_filter, bar_check
    )
    bar_durations1 = tuple(x[1] for x in bar_durations1 if x is not None)
    bar_durations2 = gutl.walk_deterministic_until_cond(
        bar_chain, bar_start, bar_filter, bar_check
    )
    bar_durations2 = tuple(x[1] for x in bar_durations2 if x is not None)

    # get rhythms
    rhy_start = (("kick", 0.125),)
    rhy_check = sutl.get_whole_dur_checker(40)
    drum_filter = sutl.get_single_dur_checker(0, 0.125, 0.0625, False)
    datas = gutl.walk_deterministic_until_cond(
        rhy_chain, rhy_start, drum_filter, rhy_check
    )
    # setup ladder
    ladder = DrumLadder()

    # tempo and dynamics
    tempo1, dynamics1 = tempynamic1
    dynamics1 = dynamics1[: len(bar_durations1)]
    dynamics1 = gutl.scale_ls(dynamics1, 2, 5)
    dynamics1 = [gutl.quantize(dyn, 1) for dyn in dynamics1]

    tempo2, dynamics2 = tempynamic2
    dynamics2 = dynamics2[: len(bar_durations2)]
    dynamics2 = gutl.scale_ls(dynamics2, 2, 5)
    dynamics2 = [gutl.quantize(dyn, 1) for dyn in dynamics2]

    # make events
    datas1 = pop_cut(bar_durations1, datas)
    events1 = ladder.climb_up_and_down(datas1)
    events1 = add_dynamics(bar_durations1, events1, dynamics1)
    events1 = support_cymbal(events1)
    sutl.append_rest_eob(events1)
    datas2 = pop_cut(bar_durations2, datas)
    events2 = ladder.climb_up(datas2)
    events2 = add_dynamics(bar_durations2, events2, dynamics2)
    events2 = support_cymbal(events2)
    sutl.append_rest_eob(events2)

    # make notes
    notes1 = nm.make_notes(events1)
    notes1.append(aj.Rest(1))
    notes2 = nm.make_notes(events2)
    notes2.append(aj.Rest(1))

    # attach part 2.1
    tempo_mark1 = aj.MetronomeMark((1, 4), tempo1)
    aj.attach(tempo_mark1, notes1[0])
    mallets = aj.Markup("with soft mallets", direction=aj.Up,)
    aj.attach(mallets, notes1[0])
    aj.attach(next(tape_counter), notes1[0])
    aj.attach(next(tape_counter), notes1[-1])
    aj.attach(aj.Fermata(command="verylongfermata"), notes1[-1])

    # attach part 2.2
    tempo_mark1 = aj.MetronomeMark((1, 4), tempo2)
    aj.attach(tempo_mark1, notes2[0])
    sticks = aj.Markup("with hard sticks", direction=aj.Up,)
    aj.attach(sticks, notes2[0])
    aj.attach(next(tape_counter), notes2[0])
    aj.attach(next(tape_counter), notes2[-1])
    aj.attach(aj.Fermata(command="verylongfermata"), notes2[-1])

    voice = aj.Voice(notes1 + notes2)
    result = aj.Container([voice])
    return result


class DrumLadder:
    ladder_instr = tuple(nm.instruments.keys())[:-1]

    def __init__(self):
        self.up()

    def climb_up_and_down(self, datas):
        return self.climb(datas, self.up_and_down)

    def climb_up(self, datas):
        return self.climb(datas, self.up)

    def climb(self, datas, func):
        result = []
        old_instr = datas[0][0]
        for d in datas:
            instr = next(self.current_ladder)
            dur = d[1]
            note = e.Event(instr, dur)
            result.append(note)
            if old_instr == d[0]:
                old_instr == d[0]
                func()
        return result

    def up_and_down(self):
        new_start = next(self.current_ladder)
        self.direction = 1 if self.direction == -1 else -1
        self.current_ladder = self.make_future_ladder(new_start, self.direction, True)

    def up(self):
        self.direction = 1
        self.current_ladder = self.make_future_ladder("bd", self.direction, False)

    @classmethod
    def make_future_ladder(cls, start, direction, mirror):
        index = cls.ladder_instr.index(start)
        rev = tuple(reversed(cls.ladder_instr[1:-1]))
        result = cls.ladder_instr[index:]
        if mirror:
            result = result + rev
        result = result + cls.ladder_instr[:index]
        if direction < 0:
            rev2 = tuple(reversed(result[1:]))
            result = result[:1] + rev2
        elif direction > 0:
            pass
        return it.cycle(result)


def pop_cut(durations, datas):
    result = []
    for dur in durations:
        bar = sutl.pop_until(dur, datas)
        bar = sutl.extend_cut_until(dur, bar)
        result.extend(bar)
    return result


def add_dynamics(durations, events, dynamics):
    stack = []
    durs = (d for d in durations)
    dur = next(durs)
    result = []
    old_dyn = None
    for event in events:
        if sum(s.delay for s in stack) >= dur:
            new_dyn = dynamics.pop(0)
            if new_dyn != old_dyn:
                stack[0].dynamic = new_dyn
                old_dyn = new_dyn
            else:
                if old_dyn != 6:
                    stack[0].dynamic = 6
                    old_dyn = 6

            result.extend(stack)
            stack = [event]
            dur = next(durs)
        else:
            stack.append(event)
    if stack:
        stack[0].dynamic = dynamics.pop(0)
        result.extend(stack)
    assert len(events) == len(result)
    return result


def support_cymbal(events):
    result = []
    instr_cycle = it.cycle(tuple(nm.instruments.keys())[:4])
    for event in events:
        if event.instr == "cy":
            result.append(e.Event(next(instr_cycle), 0))
        result.append(event)
    return result
