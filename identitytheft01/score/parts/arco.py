import abjad as aj

import identitytheft01.general.utility as gutl
import identitytheft01.score.event as e
import identitytheft01.score.utility as sutl
from identitytheft01.score.notemaker import NoteMaker as nm


def create_arco(low_chain, high_chain, tempynamic, tape_counter):
    """no hits"""

    low_start = (("kick", 1),)
    drum_filter = sutl.get_single_dur_checker(1, 32, 0.125, True)
    low_datas = gutl.walk_deterministic_until_cond(
        low_chain, low_start, drum_filter, until_none
    )
    mid_start = (("snare", 1),)
    mid__datas = gutl.walk_deterministic_until_cond(
        low_chain, mid_start, drum_filter, until_none
    )
    high_start = (("hihat", 1),)
    cymbal_filter = sutl.get_single_dur_checker(2, 32, 0.125, False)
    high_datas = gutl.walk_deterministic_until_cond(
        high_chain, high_start, cymbal_filter, lambda ls: len(ls) == 1
    )

    events_down = []
    events_up = []
    for low, mid, in zip(low_datas, mid__datas):
        if low is not None:
            low_instr = low[0]
            low_instr = "bd" if low_instr == "kick" else "ft"
            low_dur = low[1]
            events_down.append(e.Event(low_instr, low_dur, low_dur, arco=True))
        if mid is not None:
            mid_instr = mid[0]
            mid_instr = "sd" if mid_instr == "snare" else "cg"
            mid_dur = mid[1]
            events_up.append(e.Event(mid_instr, mid_dur, mid_dur, arco=True))
        if low is None or mid is None:
            high = high_datas.pop(0)
            high_dur = high[1]
            pickput_dur = 1 / 2.0
            sutl.catch_up([events_down, events_up])
            # sutl.append_rest_eob(events_down)
            # sutl.append_rest_eob(events_up)
            events_down.append(e.Event(None, pickput_dur))
            events_up.append(e.Event(None, pickput_dur))
            events_down.append(e.Event(None, high_dur))
            events_up.append(e.Event("cy", high_dur, high_dur, arco=True))
            events_down.append(e.Event(None, pickput_dur))
            events_up.append(e.Event(None, pickput_dur))
        if not high_datas:
            break

    sutl.catch_up([events_down, events_up])
    events_down = combine_durs(events_down)
    events_up = combine_durs(events_up)
    sutl.append_rest_eob(events_down)
    sutl.append_rest_eob(events_up)

    # transition to abjad
    notes_down = nm.make_notes(events_down)
    notes_up = nm.make_notes(events_up)

    # attach marks
    # tempo
    tempo = aj.MetronomeMark((1, 4), tempynamic[0])
    aj.attach(tempo, notes_up[0])
    # instruments
    arco_str = "with snare off, 2 superballs and 1 bow try to get as much sustain and release as possible"
    arco = aj.Markup(arco_str, direction=aj.Up,)
    aj.attach(arco, notes_up[0])
    # tape
    tape = next(tape_counter)
    aj.attach(tape, notes_up[-2])
    aj.attach(aj.Fermata(command="verylongfermata"), notes_up[-1])
    # simultaneous
    voice_down = aj.Voice(notes_down)
    voice_up = aj.Voice(notes_up)
    result = aj.Container([voice_down, voice_up])
    result.simultaneous = True
    return result


def combine_durs(events):
    def combine_stack(stack):
        delay = sum(n.delay for n in stack)
        dur = sum(n.duration for n in stack)
        res = e.Event(stack[0].instr, delay, dur, stack[0].dynamic, stack[0].arco)
        return res

    stack = [events[0]]
    result = []
    for note in events[1:]:
        delay = sum(n.delay for n in stack)
        if stack[0].instr == "cg" and delay >= 2:
            tmp = stack.pop(-1)
            tmp.instr = "sd"
            res = combine_stack(stack)
            result.append(res)
            stack = [tmp]
        if stack[0].instr != note.instr:
            res = combine_stack(stack)
            result.append(res)
            stack = [note]
        else:
            stack.append(note)
    res = combine_stack(stack)
    result.append(res)
    assert sum(n.delay for n in events) == sum(n.delay for n in result)
    return result


def until_none(ls):
    if not ls:
        return False
    else:
        return ls[-1] is None
