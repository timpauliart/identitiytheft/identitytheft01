import itertools as it

import abjad as aj

import identitytheft01.score.event as e
import identitytheft01.score.utility as sutl
import identitytheft01.general.utility as gutl
from identitytheft01.score.notemaker import NoteMaker as nm


def create_morecowbell(low_chain, high_chain, bar_chain, tempynamic, tape_counter):
    """end always with cowbell"""

    # get pseudo bars
    bar_start = (("kick", 1), ("kick", 1))
    bar_check = sutl.get_whole_dur_checker(32)
    bar_filter = sutl.get_single_dur_checker(0.25, 32, 0.125, False)
    bar_durations = gutl.walk_deterministic_until_cond(
        bar_chain, bar_start, bar_filter, bar_check
    )
    bar_durations = sorted(
        tuple(x[1] for x in bar_durations if x is not None), reverse=True
    )

    alternate = it.cycle(["human", "computer"])
    low_start = (("kick", 0.25), ("snare", 0.25))
    high_start = (("snare", 0.1875), ("snare", 0.0625))
    low_events = []
    high_events = []
    for bar_dur, alt in zip(bar_durations, alternate):
        whole_check = sutl.get_whole_dur_checker(bar_dur)
        quant = 1 / 16.0
        single_check = sutl.get_single_dur_checker(0, bar_dur - quant, quant, False)
        if alt == "human":
            # low
            low_datas = gutl.walk_deterministic_until_cond(
                low_chain, low_start, single_check, whole_check
            )
            low_start = (low_start[1], low_datas[-1])
            low_datas = low_datas[:-1]
            low_datas = sutl.extend_cut_until(bar_dur, low_datas)
            low_tmp_events = [low_data_to_event(d) for d in low_datas]
            low_events.extend(low_tmp_events)

            # high
            high_datas = gutl.walk_deterministic_until_cond(
                high_chain, high_start, single_check, whole_check
            )
            high_start = (high_start[1], high_datas[-1])
            high_datas = high_datas[:-1]
            high_datas = sutl.extend_cut_until(bar_dur, high_datas)
            high_tmp_events = [high_data_to_event(d) for d in high_datas]
            high_events.extend(high_tmp_events)

        else:
            low_event = e.Event(None, bar_dur)
            low_events.append(low_event)
            high_event = e.Event(high_events[-1].instr, bar_dur, arco=True)
            high_events.append(high_event)

    sutl.append_rest_eob(low_events)
    sutl.append_rest_eob(high_events)
    notes_down = nm.make_notes(low_events)
    notes_up = nm.make_notes(high_events)

    tempo, dynamics = tempynamic
    tempo_mark1 = aj.MetronomeMark((1, 4), tempo)
    aj.attach(tempo_mark1, notes_up[0])

    voice_up = aj.Voice(notes_up)
    voice_down = aj.Voice(notes_down)
    result = aj.Container([voice_down, voice_up])
    result.simultaneous = True
    return result, bar_durations


def low_data_to_event(data):
    instr = "bd" if data[0] == "kick" else "sd"
    return e.Event(instr, data[1])


def high_data_to_event(data):
    instr = "ft" if data[0] == "hihat" else "cg"
    return e.Event(instr, data[1])
