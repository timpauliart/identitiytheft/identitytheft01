import abjad as aj

import identitytheft01.score.event as e
import identitytheft01.score.utility as sutl
import identitytheft01.general.utility as gutl
from identitytheft01.score.notemaker import NoteMaker as nm


def create_rimshotmadness(
    single_chain, double_chain, bar_chain, tempynamic, tape_counter
):
    """rim shot madness / regular repititions"""
    # get pseudo bars

    bar_start = sorted(bar_chain.keys(), key=lambda key: sum(k[1] for k in key))[-1]
    bar_check = sutl.get_whole_dur_checker(32)
    bar_filter = sutl.get_single_dur_checker(0.5, 32, 0.125, False)
    bar_durations = gutl.walk_deterministic_until_cond(
        bar_chain, bar_start, bar_filter, bar_check
    )

    single_start = (("snare", 1 / 16.0), ("snare", 1 / 16.0), ("snare", 1 / 16.0))
    double_start = (("kick", 0.125), ("hihat", 0.0625), ("hihat", 0.0625))
    # single_instrs = it.cycle([])
    # double_instrs =
    low_events = []
    high_events = []
    for bar_data in bar_durations:
        whole_check = sutl.get_whole_dur_checker(bar_data[1])
        single_check = sutl.get_single_dur_checker(0, 0.125, 1 / 16.0, False)
        if bar_data[0] == "kick":
            double_datas = gutl.walk_deterministic_until_cond(
                double_chain, double_start, single_check, whole_check
            )
            double_datas = sutl.extend_cut_until(bar_data[1], double_datas)
            double_tmp_events_down = [
                double_data_to_event(d) for d in double_datas if d[0] == "kick"
            ]
            double_tmp_events_up = [
                double_data_to_event(d) for d in double_datas if d[0] == "hihat"
            ]
            low_events.extend(double_tmp_events_down)
            high_events.extend(double_tmp_events_up)
            sutl.catch_up([low_events, high_events], True)
        elif bar_data[0] == "snare":
            single_datas = gutl.walk_deterministic_until_cond(
                single_chain, single_start, single_check, whole_check
            )
            single_datas = sutl.extend_cut_until(bar_data[1], single_datas)
            single_tmp_events_up = [single_data_to_event(d) for d in single_datas]
            high_events.extend(single_tmp_events_up)
            low_event = e.Event(None, bar_data[1])
            low_events.append(low_event)
        elif bar_data[0] == "hihat":
            low_event = e.Event(None, bar_data[1])
            low_events.append(low_event)
            high_event = e.Event(None, bar_data[1])
            high_events.append(high_event)

    sutl.append_rest_eob(low_events)
    sutl.append_rest_eob(high_events)
    notes_down = nm.make_notes(low_events)
    notes_up = nm.make_notes(high_events)

    tempo, dynamics = tempynamic
    tempo_mark1 = aj.MetronomeMark((1, 4), tempo)
    # aj.attach(tempo_mark1, notes_up[0])

    voice_up = aj.Voice(notes_up)
    voice_down = aj.Voice(notes_down)
    result = aj.Container([voice_down, voice_up])
    result.simultaneous = True
    return result


def double_data_to_event(data):
    instr = "bd" if data[0] == "kick" else "sd"
    return e.Event(instr, data[1])


def single_data_to_event(data):
    instr = "ft" if data[0] == "hihat" else "cg"
    return e.Event(instr, data[1])
