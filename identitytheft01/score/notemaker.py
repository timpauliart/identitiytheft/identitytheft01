import abjad as aj
import itertools as it
import identitytheft01.score.notesplit as notesplit
import fractions as f
import math
import identitytheft01.score.event as e
import identitytheft01.general.utility as utl


class NoteMaker:
    maker = aj.LeafMaker()
    instruments = {
        "bd": -10,
        "ft": -7,
        "sd": -3,
        "cg": 0,
        "cb": 4,
        "cy": 7,
        "cl": 11,
    }

    grids = (1, 0.25)

    dynamics = ("ppp", "pp", "p", "mp", "mf", "f", "ff", "fff")

    @classmethod
    def make_notes(cls, events):
        events = cls.merge_rests(events)
        times = utl.delays_to_times([n.delay for n in events])
        chord = []
        result = []
        for note, time in zip(events, times):
            # get the real instrument name
            if note.instr not in cls.instruments:
                pitch = None
            else:
                pitch = cls.instruments[note.instr]

            # handle possible chord
            if note.delay == 0:
                chord.append(pitch)
            else:
                if chord:
                    pitches = [(pitch,) + tuple(chord)]
                    chord = []
                else:
                    pitches = [pitch]

                # handle durations
                note_durs = cls.split_dur(time, note.duration, cls.grids)
                rest_dur = note.delay - note.duration
                if not note.arco:
                    rest_dur += sum(note_durs[1:])
                    note_durs = note_durs[:1]

                # create
                pitches = cls.maker(pitches, note_durs)
                # pitches = cls.insert_skips(pitches)
                # tie
                [aj.attach(aj.Tie(), p) for p in pitches[:-1]]
                # add dynamic
                if note.dynamic is not None:
                    dyn = cls.dynamics[note.dynamic]
                    aj.attach(aj.Dynamic(dyn), pitches[0])
                result.extend(pitches)
                # add extra rests
                if rest_dur > 0:
                    rest_durs = cls.split_dur(
                        time + sum(note_durs), rest_dur, cls.grids
                    )
                    rests = cls.maker([None], rest_durs)
                    # rests = cls.insert_skips(rests)
                    result.extend(rests)
        return result

    @classmethod
    def split_dur(cls, time, dur, grid_units):
        if not grid_units:
            return tuple(
                aj.Duration(d) for d in notesplit.seperate_by_assignability(dur, 1)
            )
        else:
            # setup
            start = f.Fraction(time)
            end = time + dur
            grid_unit = grid_units[0]
            grid = tuple(
                f.Fraction(grid_unit) for i in range(math.ceil(end / grid_unit))
            )
            end = f.Fraction(end)

            # make it
            durs = notesplit.seperate_by_grid(start, end, grid)
            times = utl.delays_to_times(durs, start)
            durs = [cls.split_dur(t, d, grid_units[1:]) for t, d in zip(times, durs)]
            durs = [ls for sublist in durs for ls in sublist]
            return durs

    @classmethod
    def merge_rests(cls, notes):
        result = []
        stack = []
        for note in notes:
            if note.instr is None:
                stack.append(note.delay)
            else:
                rest_dur = sum(stack)
                stack = []
                if rest_dur > 0:
                    result.append(e.Event(None, rest_dur, rest_dur))
                result.append(note)
        rest_dur = sum(stack)
        if rest_dur > 0:
            result.append(e.Event(None, rest_dur, rest_dur))
        assert sum(n.delay for n in notes) == sum(r.delay for r in result)
        return result

    @classmethod
    def insert_skips(cls, notes):
        def skip(note):
            note = aj.inspect(note).leaf(0)
            if type(note) is aj.Rest and note.written_duration >= 1:
                return aj.Skip(note.written_duration)
            else:
                return note

        result = [skip(note) for note in notes]
        return result

    @classmethod
    def create_tape_counter(cls):
        def tape(no):
            result = aj.Markup(r"\rounded-box " + "tape" + str(no), direction=aj.Up)
            return result

        def tape_counter_generator():
            for i in it.count(1):
                yield tape(i)

        return tape_counter_generator()
