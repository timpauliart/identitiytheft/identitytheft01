import math

import identitytheft01.score.event as e
import identitytheft01.general.utility as gutl


# operation with datas
def extend_cut_until(duration, datas):
    result = [data for data in datas]
    old_duration = sum(r[-1] for r in result)
    diff = duration - old_duration
    if result:
        result[-1] = (result[-1][0], result[-1][1] + diff)
    return result


def pop_until(duration, datas):
    duration_reached = 0
    result = []
    while duration_reached < duration:
        data = datas.pop(0)
        result.append(data)
        duration_reached += data[1]
    return result


def get_single_dur_checker(min_dur, max_dur, quant, no):
    def dur_checker(x):
        if x is None:
            return no
        min_cond = x[1] >= min_dur
        max_cond = x[1] <= max_dur
        if quant is None:
            quant_cond = True
        else:
            quant_cond = gutl.quantize(x[1], quant) == x[1]
        return min_cond and max_cond and quant_cond

    return dur_checker


def get_whole_dur_checker(min_dur):
    def dur_checker(ls):
        dur = sum(x[1] for x in ls)
        return dur >= min_dur

    return dur_checker


def get_whole_len_checker(length):
    def len_checker(ls):
        return len(ls) == length

    return len_checker


# operation with events
# this works only for 4/4
def append_rest_eob(events):
    voice_dur = sum(n.delay for n in events)
    dur = math.ceil(voice_dur) - voice_dur
    if dur > 0:
        events.append(e.Event(None, dur))


def catch_up(events, appendrest: bool = False):
    durs = [sum(note.delay for note in voice) for voice in events]
    longest = max(durs)
    rest_durs = [longest - dur for dur in durs]
    for rest_dur, voice in zip(rest_durs, events):
        if rest_dur > 0:
            if appendrest:
                rest = e.Event(None, rest_dur)
                voice.append(rest)
            else:
                voice[-1].delay = voice[-1].delay + rest_dur
                voice[-1].duration = voice[-1].duration + rest_dur
