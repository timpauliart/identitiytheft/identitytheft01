import math
import os

import bpy


def import_meshes(folder):
    for file in os.listdir(folder):
        if file.endswith(".obj"):
            filename = os.path.join(folder, file)
            bpy.ops.import_scene.obj(filepath=filename)


def basic_world():
    # delete default cube
    cube = bpy.data.objects["Cube"]
    cube.select_set(True)
    bpy.ops.object.delete()

    # light
    light = bpy.data.objects["Light"]
    light.select_set(True)
    bpy.ops.object.origin_set(type="GEOMETRY_ORIGIN", center="BOUNDS")
    light.select_set(False)
    light.location = (0, 0, 4)
    light.rotation_euler = (0, 0, 0)
    light.data.type = "SPOT"
    light = bpy.data.lights["Light"]
    light.spot_size = math.radians(22)

    # camera
    camera = bpy.data.objects["Camera"]
    camera.select_set(True)
    bpy.ops.object.origin_set(type="GEOMETRY_ORIGIN", center="BOUNDS")
    camera.location = (0, -4, 1)
    camera.rotation_euler = (math.radians(90.0), math.radians(90.0), 0)
    camera.select_set(False)

    # material
    black = bpy.data.materials.new(name="black")
    black.diffuse_color = (0, 0, 0, 0)
    black.metallic = 0
    black.specular_intensity = 1
    black.roughness = 1

    size = 100.0
    # floor
    bpy.ops.mesh.primitive_plane_add(size=size)
    bpy.context.object.data.materials.append(black)
    floor = bpy.data.objects["Plane"]
    floor.name = "Floor"

    # wall
    bpy.ops.mesh.primitive_plane_add(
        size=size,
        location=(0, size / 2.0, size / 2.0),
        rotation=(math.radians(90.0), math.radians(90.0), 0),
    )
    bpy.context.object.data.materials.append(black)
    wall = bpy.data.objects["Plane"]
    wall.name = "Wall"


def create_heads():
    heads = bpy.data.collections.new("heads")
    bpy.context.scene.collection.children.link(heads)
    layer_heads = bpy.context.view_layer.layer_collection.children[heads.name]
    bpy.context.view_layer.active_layer_collection = layer_heads

    dir1 = "models/clean/10essentials"
    # import_meshes(dir1)
    dir2 = "models/clean/ontherocks"
    # import_meshes(dir2)
    testdir = "models/clean/notused"
    import_meshes(testdir)

    bpy.ops.object.select_all(action="DESELECT")
    for obj in heads.objects:
        obj.select_set(True)
        bpy.ops.object.origin_set(type="GEOMETRY_ORIGIN", center="BOUNDS")
        obj.location = (0, 0, 1)
        factor = min([1.125 / obj.dimensions.x, 2.0 / obj.dimensions.z])
        bpy.ops.transform.resize(value=(factor, factor, factor))
        obj.select_set(False)
