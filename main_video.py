import os
import sys
import bpy

dir = os.path.dirname(bpy.data.filepath)
if dir not in sys.path:
    sys.path.append(dir)

import identitytheft01.video.basics as vb

vb.basic_world()
vb.create_heads()
