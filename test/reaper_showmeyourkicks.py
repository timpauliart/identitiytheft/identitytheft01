import os
import pathlib as pl
import itertools as it

import reapy
from reapy import reascript_api as rpr

import identitytheft01.audio.reaper as rp
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl


def create_showmeyourkicks(rhy_chain):
    """lots of kicks and 808s"""

    # get the soundfiles
    kickfolder = os.getcwd() + "/samples/kick"
    bassfolder = os.getcwd() + "/samples/808"
    kickfiles = sorted(tuple(pl.Path(kickfolder).glob("*")))
    bassfiles = sorted(tuple(pl.Path(bassfolder).glob("*")))
    soundfiles = sorted(kickfiles + bassfiles)

    start = (("kick", 1 / 32.0),)
    whole_check = sutl.get_whole_len_checker(len(soundfiles))
    single_check = sutl.get_single_dur_checker(0, 1 / 16.0, None, False)
    rhy = gutl.walk_deterministic_until_cond(
        rhy_chain, start, single_check, whole_check
    )
    rhy = [r[1] for r in rhy]

    kick_rhy1 = tuple(it.islice(rhy, 0, None, 2))
    bass_rhy1 = tuple(it.islice(rhy, 1, None, 2))
    kick_rhy2 = [k + b for k, b in zip(kick_rhy1, bass_rhy1)]
    bass_rhy2 = [k + b for k, b in zip(kick_rhy1[1:], bass_rhy1)]

    with reapy.inside_reaper():
        kick = rp.create_track("kick")
        rp.sequence(kick, kick_rhy2, it.cycle(kickfiles))

        bass = rp.create_track("808")
        rp.sequence(bass, bass_rhy2, bassfiles)

        rpr.UpdateArrange()


# snare = rp.create_track("snare")
# snarefolder = os.getcwd() + "/samples/snare"
# rp.sequence(snare, snare_rhy, snarefolder)

# hihat = rp.create_track("hihat")
# hihatfolder = os.getcwd() + "/samples/hihat"
# rp.sequence(hihat, hihat_rhy, hihatfolder)
