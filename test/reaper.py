from reapy import reascript_api as rpr


def insert_media(track, soundfile, startinsec):
    print(soundfile)
    rpr.SetOnlyTrackSelected(track)
    rpr.SetEditCurPos(startinsec, False, False)
    rpr.InsertMedia(soundfile, 0)
    media_item = rpr.AddMediaItemToTrack(track)
    take = rpr.AddTakeToMediaItem(media_item)

    rpr.BR_SetTakeSourceFromFile(take, soundfile, False)
    # rpr.GetSetMediaItemTakeInfo_String(take, parmname, stringNeedBig, setNewValue)
    rpr.SetMediaItemInfo_Value(media_item, "D_LENGTH", 1)

    # print(rpr.GetMediaSourceNumChannels(pcm))

    # (Float retval, PCM_source source, Boolean lengthIsQNOut) = RPR_GetMediaSourceLength(source, lengthIsQNOut)


def create_track(name=""):
    rpr.InsertTrackAtIndex(0, False)
    track = rpr.GetTrack(0, 0)
    rpr.GetSetMediaTrackInfo_String(track, "P_NAME", name, True)
    return track


def create_sound(track, start, dur, soundfile, fade_factor, index):
    """"in"""
    # convert to seconds
    print(str(soundfile))
    print(start)
    print(dur)
    startinsec = rpr.TimeMap_QNToTime(4 * start)
    endinsec = rpr.TimeMap_QNToTime(4 * (start + dur))
    durinsec = endinsec - startinsec
    print(startinsec)
    print(durinsec)

    rpr.SetOnlyTrackSelected(track)
    rpr.SetEditCurPos(startinsec, False, False)
    rpr.InsertMedia(soundfile, 0)
    media_item = rpr.GetTrackMediaItem(track, index)
    print(media_item)
    length = rpr.GetMediaItemInfo_Value(media_item, "D_LENGTH")
    print(length)
    if length > durinsec:
        rpr.SetMediaItemInfo_Value(media_item, "D_LENGTH", durinsec)
    fade = fade_factor * durinsec
    rpr.SetMediaItemInfo_Value(media_item, "D_FADEOUTLEN", fade)
    return media_item


def sequence(track, start, durs, soundfiles, startindex):
    time = start
    items = []
    for dur, soundfile, index in zip(
        durs, soundfiles, range(startindex, startindex + len(durs))
    ):
        media_item = create_sound(track, time, dur, str(soundfile), 0.01, index)
        items.append(media_item)
        time += dur

    return items
