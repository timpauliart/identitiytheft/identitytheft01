extractframes() {
  for video in *.{webm,mkv,mp4}; do
    # get name
    dest=${dest##*/}
    dest=${video%%.*}
    echo $dest
    # extract frames
    orig="$dest/orig"
    mkdir -p "$orig"
    ffmpeg -i "$video" -vf fps=1/5 "$orig/img%d.png"
    # crop frames
    echo "crop frames"
    crop="$dest/crop"
    mkdir -p "$crop"
    for img in $orig/*.png; do
      name=${img##*/}
      facedetect --biggest "$img" | while read x y w h; do
        convert "$img" -crop $((w * 2))x$((h * 2))+$((x - (w / 2)))+$((y - (h / 2))) "$crop/$name"
      done
    done
    # remove background
    echo "remove background"
    alpha="$dest/alpha"
    mkdir -p "$alpha"
    for img in $crop/*.png; do
      name=${img##*/}
      color=$(convert $img -format "%[pixel:p{0,0}]" info:-)
      convert $img -fuzz 5% -transparent $color "$alpha/$name"
    done
  done
}

cd 10essentials

extractframes

cd ../ontherocks

extractframes
